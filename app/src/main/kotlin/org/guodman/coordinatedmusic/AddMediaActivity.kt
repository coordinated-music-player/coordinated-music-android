package org.guodman.coordinatedmusic

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_media.*
import java.util.*

class AddMediaActivity : AppCompatActivity() {
    var queues: List<String> = ArrayList<String>()

    private val queuesCallback: LoaderManager.LoaderCallbacks<List<String>> = object: LoaderManager.LoaderCallbacks<List<String>> {
        override fun onCreateLoader(id: Int, args: Bundle?): Loader<List<String>>? {
            return PlayQueuesLoader(this@AddMediaActivity)
        }

        override fun onLoadFinished(loader: Loader<List<String>>?, data: List<String>?) {
            Log.d(TAG, "Queues Loaded: ${data}")
            val mutQueue = (data ?: ArrayList()).toMutableList()
            mutQueue.add(0, "New Queue")
            queues = mutQueue.toList()
            val adapter = ArrayAdapter<String>(this@AddMediaActivity,
                    android.R.layout.simple_spinner_item, queues)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            queueName.setAdapter(adapter)
        }

        override fun onLoaderReset(loader: Loader<List<String>>?) {
            val mutList = ArrayList<String>()
            mutList.add("New Queue")
            queues = mutList
            val adapter = ArrayAdapter<String>(this@AddMediaActivity,
                    android.R.layout.simple_spinner_item, queues)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            queueName.setAdapter(adapter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_media)

        val slug: String? = intent?.getStringExtra("slug")
        if (slug == null) {
            finish()
            return
        }

        val intentMediaTitle: String? = intent?.getStringExtra("title")

        mediaTitle.text = intentMediaTitle
        val adapter = ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, queues)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        queueName.setAdapter(adapter)

        cancel.setOnClickListener {
            finish()
        }

        queueName.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                queueNameNew.visibility = View.VISIBLE
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val q = queues.get(position)
                if (q == "New Queue") {
                    queueNameNew.visibility = View.VISIBLE
                } else {
                    queueNameNew.visibility = View.GONE
                }
            }
        }

        add.setOnClickListener {
            val queueName: String? = if (queueNameNew.visibility == View.VISIBLE) {
                queueNameNew.text.toString()
            } else {
                queues.get(queueName.selectedItemPosition)
            }
            if (queueName != null && queueName.isNotEmpty()) {
                appendToQueue(this@AddMediaActivity, queueName, slug, {
                    supportLoaderManager.restartLoader(LOADER_QUEUES, null, queuesCallback)
                    finish()
                }, { msg ->
                    Log.w(TAG, msg)
                    runOnUiThread {
                        Toast.makeText(this@AddMediaActivity, msg, Toast.LENGTH_LONG).show()
                    }
                })
            }
        }

        replace.setOnClickListener {
            val queueName: String? = if (queueNameNew.visibility == View.VISIBLE) {
                queueNameNew.text.toString()
            } else {
                queues.get(queueName.selectedItemPosition)
            }
            if (queueName != null && queueName.isNotEmpty()) {
                replaceQueue(this@AddMediaActivity, queueName, slug, {
                    supportLoaderManager.restartLoader(LOADER_QUEUES, null, queuesCallback)
                    finish()
                }, { msg ->
                    Log.w(TAG, msg)
                    runOnUiThread {
                        Toast.makeText(this@AddMediaActivity, msg, Toast.LENGTH_LONG).show()
                    }
                })
            }
        }
    }

    override fun onResume() {
        super.onResume()
        supportLoaderManager.restartLoader(LOADER_QUEUES, null, queuesCallback)
    }

    companion object {
        fun createIntent(context: Context, slug: String, title: String): Intent {
            val i = Intent(context, AddMediaActivity::class.java)
            i.putExtra("slug", slug)
            i.putExtra("title", title)
            return i
        }
    }
}
