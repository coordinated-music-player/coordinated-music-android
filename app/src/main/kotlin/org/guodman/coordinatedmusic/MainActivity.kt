package org.guodman.coordinatedmusic

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.os.SystemClock
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.beust.klaxon.array
import kotlinx.android.synthetic.main.activity_main.*
import org.guodman.coordinatedmusic.model.PlayQueue
import org.guodman.coordinatedmusic.model.QueueItem
import org.guodman.coordinatedmusic.model.generatePlayQueue
import java.util.*

class MainActivity : AppCompatActivity() {
    val TAG = "MainActivity"
    var agents: List<String> = ArrayList<String>()
    var selectedAgent: String? = null
    var queues: List<String> = ArrayList<String>()
    var playQueueAdapter: PlayQueueAdapter? = null
    var ws: SteadySocket? = null
    var seekBarInfo: SeekBarInfo = SeekBarInfo(AgentAPI.PlayState.STOP, 0.0)
    var positionLoop: Runnable? = null

    private val agentsCallbacks: LoaderManager.LoaderCallbacks<List<String>> = object: LoaderManager.LoaderCallbacks<List<String>> {
        override fun onCreateLoader(id: Int, args: Bundle?): Loader<List<String>>? {
            return AgentsLoader(this@MainActivity)
        }

        override fun onLoadFinished(loader: Loader<List<String>>?, data: List<String>?) {
            Log.d(TAG, "Agents Loaded: ${data}")
            agents = data ?: ArrayList()

            val agent = GeneralPrefs.getCurrentAgent(this@MainActivity)
            if (agents.contains(agent)) {
                selectAgent(agent)
            }
        }

        override fun onLoaderReset(loader: Loader<List<String>>?) {
            agents = ArrayList()
        }
    }

    private val queuesCallbacks: LoaderManager.LoaderCallbacks<List<String>> = object: LoaderManager.LoaderCallbacks<List<String>> {
        override fun onCreateLoader(id: Int, args: Bundle?): Loader<List<String>>? {
            return PlayQueuesLoader(this@MainActivity)
        }

        override fun onLoadFinished(loader: Loader<List<String>>?, data: List<String>?) {
            Log.d(TAG, "Queues Loaded: ${data}")
            queues = data ?: ArrayList()
        }

        override fun onLoaderReset(loader: Loader<List<String>>?) {
            queues = ArrayList()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val agentsAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, agents)
        agentsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        supportLoaderManager.initLoader(LOADER_AGENTS, null, agentsCallbacks)

        songsView.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        songsView.layoutManager = layoutManager
        playQueueAdapter = PlayQueueAdapter(layoutManager)
        songsView.adapter = playQueueAdapter

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {}

            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {
                val pos = p0?.progress
                val a = selectedAgent
                if (pos != null && a != null) {
                    Thread {
                        sendSeek(this@MainActivity, a, pos)
                    }.start()
                }
            }
        })

        startAgent.setOnClickListener {
            MediaService.load(this)
        }

        stopAgent.setOnClickListener {
            MediaService.disconnect(this)
        }

        addMedia.setOnClickListener {
            startActivity(createSelectMediaIntent(this, playQueueAdapter?.queue?.name ?: "Default", ArrayList<String>()))
        }

        editQueue.setOnClickListener {
            val q = playQueueAdapter?.queue
            if (q != null) {
                startActivity(createEditQueueIntent(this, q))
            }
        }

        agentPicker.setOnClickListener {
            val popup = PopupMenu(this, agentPicker)
            agents.forEach {
                popup.menu.add(it)
            }
            popup.show()
            popup.setOnMenuItemClickListener { item ->
                Toast.makeText(this, "You clicked : ${item?.title}", Toast.LENGTH_SHORT).show()
                selectAgent(item?.title?.toString())
                true
            }
        }

        currentQueue.setOnClickListener {
            val popup = PopupMenu(this, currentQueue)
            queues.forEach {
                popup.menu.add(it)
            }
            popup.show()
            popup.setOnMenuItemClickListener { item ->
                Toast.makeText(this, "You clicked : ${item.title}", Toast.LENGTH_SHORT).show()
                val agent = selectedAgent
                if (agent != null) {
                    Thread {
                        sendPlayQueue(this, agent, item.title.toString())
                    }.start()
                }
                true
            }
        }

        play.setOnClickListener {
            val a = selectedAgent
            if (a != null) {
                Thread {
                    sendPlay(this, a)
                }.start()
            }
        }

        pause.setOnClickListener {
            val a = selectedAgent
            if (a != null) {
                Thread {
                    sendPause(this, a)
                }.start()
            }
        }

        next.setOnClickListener {
            val a = selectedAgent
            if (a != null) {
                Thread {
                    sendNext(this, a)
                }.start()
            }
        }

        prev.setOnClickListener {
            val a = selectedAgent
            if (a != null) {
                Thread {
                    sendPrev(this, a)
                }.start()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        supportLoaderManager.restartLoader(LOADER_QUEUES, null, queuesCallbacks)

        val server = GeneralPrefs.getActiveServer(this)
        val url = "${server.host}/interface/connect"
        this.ws = SteadySocket(url, { me ->
            me.sendText(InterfaceAPI.COMMAND_REGISTER("AndroidUI").toJsonString())
        })
        ws?.listener = socketListener

        positionLoop = object: Runnable {
            override fun run() {
                while (positionLoop == this) {
                    val position: Double = seekBarInfo.estimateCurrentSeconds()
                    val max: Double = playQueueAdapter?.queue?.getCurrentSong()?.secondsDuration ?: 0.0
                    if (max > 0.0) {
                        runOnUiThread {
                            seekBar.max = max.toInt()
                            seekBar.progress = position.toInt()
                            seekBar.visibility = View.VISIBLE
                            timeElapsed?.text = displayableTime(position.toInt())
                            timeRemaining?.text = displayableTime(max.toInt() - position.toInt())
                            val elapsed = (playQueueAdapter?.queue?.durationFromDoneSongs() ?: 0) + position.toInt()
                            val total = playQueueAdapter?.queue?.totalDuration() ?: 0
                            totalTimeElapsed?.text = displayableTime(elapsed)
                            totalTimeRemaining?.text = displayableTime(total - elapsed)
                            totalTimeTotal?.text = displayableTime(total)
                        }
                    } else {
                        runOnUiThread {
                            seekBar.visibility = View.INVISIBLE
                            timeRemaining?.text = "0:00"
                            timeTotal?.text = "0:00"
                        }
                    }
                    SystemClock.sleep(100)
                }
                runOnUiThread {
                    timeElapsed?.text = "0:00"
                    timeRemaining?.text = "0:00"
                    timeTotal?.text = "0:00"
                    totalTimeElapsed?.text = "0:00"
                    totalTimeRemaining?.text = "0:00"
                    totalTimeTotal?.text = "0:00"
                }
            }
        }
        Thread(positionLoop).start()

        val agent = GeneralPrefs.getCurrentAgent(this@MainActivity)
        if (agents.contains(agent)) {
            selectAgent(agent)
        }
    }

    override fun onPause() {
        super.onPause()
        ws?.disconnect()
        positionLoop = null
    }

    fun updateUI() {
        runOnUiThread {
            val name = selectedAgent
            if (name != null) {
                agentPicker.text = "Agent [${agents.size}]: ${name}"
                playQueueAdapter?.clearList()
                currentQueue.text = "Select Queue"
                GeneralPrefs.saveCurrentAgent(this, name)
                startListen(name)
            } else {
                agentPicker.text = "Select Agent [${agents.size}]"
                playQueueAdapter?.clearList()
                currentQueue.text = "Select Queue"
                timeElapsed?.text = "0:00"
                timeRemaining?.text = "0:00"
                timeTotal?.text = "0:00"
            }
        }
    }

    fun selectAgent(name: String?) {
        Log.d(TAG, "Selecting agent ${name}")
        selectedAgent = name
        updateUI()
    }

    fun useQueueData(queue: PlayQueue) {
        Log.d(TAG, "useQueueData")
        Log.d(TAG, "adapter is ${playQueueAdapter ?: "no here"}")
        currentQueue.text = queue.name
        val adapter = playQueueAdapter
        if (adapter != null) {
            Log.d(TAG, "doing it!")
            playQueueAdapter?.replacePlaylist(queue)
        } else {
            Log.d(TAG, "wat!")
        }
        Toast.makeText(this@MainActivity, "Playlist Loaded", Toast.LENGTH_SHORT).show()
    }

    inner class PlayQueueAdapter(val layoutManager: LinearLayoutManager): RecyclerView.Adapter<PlayQueueAdapter.PlayQueueViewHolder>() {
        var queue: PlayQueue? = null
        var currentSong = queue?.song ?: 0

        override fun getItemCount(): Int {
            return queue?.songs?.size ?: 0
        }

        override fun onBindViewHolder(holder: PlayQueueViewHolder?, position: Int) {
            val queue = this.queue
            if (queue != null) {
                holder?.setData(queue.songs[position], position, queue.song == position)
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PlayQueueViewHolder {
            val view = LayoutInflater.from(parent?.context).inflate(R.layout.card_play_queue, parent, false) as RelativeLayout
            return PlayQueueViewHolder(view)
        }

        fun clearList() {
            queue = null
            currentSong = 0
            notifyDataSetChanged()
        }

        fun notifySongChange() {
            val queue = this.queue
            if (queue != null) {
                Log.d(TAG, "Notification that we should change the active song")
                notifyItemChanged(currentSong)
                notifyItemChanged(queue.song)
                currentSong = queue.song
                scrollToCurrent()
                timeTotal?.text = displayableTime(queue.getCurrentSong()?.secondsDuration?.toInt() ?: 0)
            }
        }

        fun scrollToCurrent() {
            val song = queue?.song
            if (song != null) {
                layoutManager.scrollToPosition(song)
            }
        }

        fun replacePlaylist(queue: PlayQueue) {
            val oldPosition = currentSong
            this.queue = queue
            currentSong = queue.song
            runOnUiThread {
                Log.d(TAG, "Forgetting about highlighting ${oldPosition} and moving to ${currentSong}")
                notifyItemChanged(oldPosition)
                notifyItemChanged(currentSong)
                notifyDataSetChanged()
                scrollToCurrent()
                timeTotal?.text = displayableTime(queue.getCurrentSong()?.secondsDuration?.toInt() ?: 0)
            }
        }

        inner class PlayQueueViewHolder(val view: ViewGroup): RecyclerView.ViewHolder(view) {
            var songName: String? = null
            var songPosition: Int? = null
            var isCurrent: Boolean = false

            fun setData(name: QueueItem, position: Int, isCurrent: Boolean) {
                Log.d(TAG, "Setting data for ${position} : ${isCurrent}, ${currentSong}")
                songName = name.path
                songPosition = position
                this.isCurrent = isCurrent

                val title = view.findViewById(R.id.songName) as TextView
                val album = view.findViewById(R.id.album) as TextView
                val artist = view.findViewById(R.id.artist) as TextView
                val download = view.findViewById(R.id.download) as TextView
                album.text = "${name.album}${ if (name.track > 0) { " | ${name.track}"} else {""} }"
                artist.text = "${displayableTime(name.secondsDuration.toInt())} | ${name.artist}"
                download.text = "dl:%d%%".format((name.cacheState * 100.0).toInt())
                if (isCurrent) {
                    title.setTypeface(Typeface.DEFAULT_BOLD, Typeface.BOLD)
                    title.text = name.title
                } else {
                    title.setTypeface(Typeface.DEFAULT, Typeface.NORMAL)
                    title.text = name.title
                }
                if (position % 2 == 0) {
                    view.setBackgroundColor(Color.parseColor("#efefef"))
                } else {
                    view.setBackgroundColor(Color.parseColor("#ffffff"))
                }
            }

            init {
                view.setOnClickListener {
                    val position = songPosition
                    val a = selectedAgent
                    if (position != null && a != null) {
                        Thread {
                            sendJump(view.context, a, position)
                        }.start()
                    }
                }

                view.setOnLongClickListener {
                    val position = songPosition
                    if (position != null) {
                        val song = queue?.songs?.get(position)
                        if (song != null) {
                            startActivity(AddMediaActivity.createIntent(this@MainActivity, song.path, song.title))
                        }
                    }
                    true
                }
            }
        }

        fun requestUpdates(updates: List<Int>) {
            updates.forEach {
                notifyItemChanged(it)
            }
        }
    }

    val socketListener: (String) -> Unit = { payload ->
        val cmd: JsonObject? = Parser().parse(payload.byteInputStream()) as? JsonObject
        Log.d(TAG, "Command: ${cmd?.get("command")} :: ${cmd}")
        when (cmd?.get("command")) {
            "agents_change" -> {
                Log.d(TAG, "Agents Change command: '${cmd?.toJsonString()}'")
                agents = cmd?.array<String>("agents") ?: ArrayList()
                val lastAgent = GeneralPrefs.getCurrentAgent(this)
                Log.d(TAG, "Try to load agent ${lastAgent}")
                if (selectedAgent == null && lastAgent != null && agents.contains(lastAgent)) {
                    selectAgent(lastAgent)
                } else if (!agents.contains(selectedAgent)) {
                    selectAgent(null)
                } else {
                    updateUI()
                }
            }
            "agent_update" -> {
                val seconds = cmd?.get("seconds") as? Double ?: (cmd?.get("seconds") as? Int)?.toDouble()
                val song = cmd?.get("song") as? Int
                runOnUiThread {
                    val q = playQueueAdapter?.queue
                    if (q != null) {
                        if (seconds != null || song != null) {
                            q.song = song ?: q.song
                            q.seconds = seconds ?: q.seconds
                            playQueueAdapter?.notifySongChange()
                        }
                    }
                }
            }
            "update_state" -> {
                val newState: AgentAPI.PlayState? = AgentAPI.getPlayStateFromString(cmd?.get("playState") as? String)
                val seconds: Double = cmd?.get("seconds") as? Double ?: (cmd?.get("seconds") as? Int)?.toDouble() ?: 0.0
                if (newState != null) {
                    seekBarInfo = SeekBarInfo(newState, seconds)
                }

                val updates = ArrayList<Int>()
                (cmd?.get("cache") as? JsonArray<*>)?.value?.forEach({
                    val js = it as? JsonObject
                    val slug: String? = js?.get("slug") as? String
                    val state: Double = js?.get("state") as? Double ?: (js?.get("state") as? Int)?.toDouble() ?: 0.0
                    if (slug != null) {
                        val matches = playQueueAdapter?.queue?.findAll(slug)
                        matches?.forEach {
                            if (it.second.setCacheState(state)) {
                                updates.add(it.first)
                            }
                        }
                    }
                })
                playQueueAdapter?.requestUpdates(updates)
            }
            "queue" -> {
                val queueJson = cmd?.get("queue") as? JsonObject
                Log.d(TAG, "queueJson: ${queueJson}")
                if (queueJson != null) {
                    runOnUiThread {
                        useQueueData(generatePlayQueue(queueJson))
                    }
                }
            }
            "info" -> {
                Log.d(TAG, "Info command: '${cmd?.toJsonString()}'")
                Log.d(TAG, cmd?.get("message") as? String ?: "No message found in info command: '${cmd?.toJsonString()}'")
            }
            "error" -> {
                Log.d(TAG, "Error command: '${cmd?.toJsonString()}'")
                Log.d(TAG, cmd?.get("message") as? String ?: "No message found in error command: '${cmd?.toJsonString()}'")
            }
        }
    }

    fun startListen(agent: String) {
        ws?.sendText(InterfaceAPI.COMMAND_LISTEN_TO_AGENT(agent).toJsonString())
    }

    fun displayableTime(seconds: Int): String {
        val hours = seconds / 3600
        val minutes = (seconds % 3600) / 60
        val justSeconds = seconds % 60
        return if (hours == 0) {
            String.format("${seconds / 60}:%1$02d", seconds % 60)
        } else {
            String.format("${hours}:%1$02d:%2$02d", minutes, justSeconds)
        }
    }
}

fun createMainActivityIntent(c: Context): Intent {
    val i = Intent(c, MainActivity::class.java)
    return i
}

class SeekBarInfo(val state: AgentAPI.PlayState, val initialSongPosition: Double) {
    val initialTime: Double = SystemClock.elapsedRealtime().toDouble() / 1000.0

    fun estimateCurrentSeconds(): Double {
        if (state == AgentAPI.PlayState.PLAY) {
            val now = SystemClock.elapsedRealtime().toDouble() / 1000.0
            val timePassed = now - initialTime
            return initialSongPosition + timePassed
        } else {
            return initialSongPosition
        }
    }
}
