package org.guodman.coordinatedmusic.model

import com.beust.klaxon.JsonObject
import com.beust.klaxon.int
import com.beust.klaxon.string

data class MediaFile(val path: String, val fullPath: String, val title: String, val track: Int) {
}

fun generateMediaFile(json: JsonObject): MediaFile {
    val path = json.string("path") ?: ""
    val parent = json.string("parent") ?: ""
    val slug = json.string("slug") ?: ""
    val titleMaybe = json.string("title")
    val title = if (titleMaybe != null && titleMaybe.isNotEmpty()) { titleMaybe } else { path }
    val track = json.int("track") ?: 0
    return MediaFile(path, slug, title, track)
}
