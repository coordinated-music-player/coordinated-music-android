package org.guodman.coordinatedmusic.model

import com.beust.klaxon.JsonObject
import com.beust.klaxon.array
import com.beust.klaxon.string
import java.util.*

data class MediaFolder(val slug: String, val path: String, val children: List<MediaFolder>, val media: List<MediaFile>) {
    fun getAllMediaFiles(): List<MediaFile> {
        return media + children.flatMap { it.getAllMediaFiles() }
    }
}

fun generateBaseMediaFolder(json: JsonObject): MediaFolder {
    val path = json.string("path") ?: ""
    val slug = json.string("slug") ?: path
    val children = json.array<JsonObject>("children")?.map {
        generateMediaFolder(it, null)
    } ?: ArrayList<MediaFolder>()
    val media = json.array<JsonObject>("music")?.map {
        generateMediaFile(it)
    } ?: ArrayList<MediaFile>()
    return MediaFolder(slug, path, children, media)
}

fun generateMediaFolder(json: JsonObject, basePath: String?): MediaFolder {
    val path = json.string("path") ?: ""
    val slug = json.string("slug") ?: path
    val fullPath = if (basePath != null) { "${basePath}/${path}" } else { path }
    val children = json.array<JsonObject>("children")?.map {
        generateMediaFolder(it, fullPath)
    } ?: ArrayList<MediaFolder>()
    val media = json.array<JsonObject>("music")?.map {
        generateMediaFile(it)
    } ?: ArrayList<MediaFile>()
    return MediaFolder(slug, path, children, media)
}
