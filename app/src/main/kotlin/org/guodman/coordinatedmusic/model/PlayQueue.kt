package org.guodman.coordinatedmusic.model

import android.util.Log
import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import org.guodman.coordinatedmusic.MediaCache
import org.guodman.coordinatedmusic.TAG
import java.util.*

class PlayQueue(val name: String, val songs: List<QueueItem>, var song: Int, var seconds: Double) {
    fun getCurrentSong(): QueueItem? {
        try {
            return songs.get(song)
        } catch (e: IndexOutOfBoundsException) {
            return null
        }
    }

    fun getCurrentSongPath(): String? {
        try {
            return songs.get(song).path
        } catch (e: IndexOutOfBoundsException) {
            return null
        }
    }

    fun getNextSongPath(): String? {
        try {
            return songs.get(song + 1).path
        } catch (e: IndexOutOfBoundsException) {
            return null
        }
    }

    fun durationFromDoneSongs(): Int {
        return songs.slice(0..song-1).fold(0) { total, next ->
            total + next.secondsDuration.toInt()
        }
    }

    fun totalDuration(): Int {
        return songs.fold(0) { total, next ->
            total + next.secondsDuration.toInt()
        }
    }

    fun getMilliseconds(): Int {
        return (seconds * 1000).toInt()
    }

    fun hasPrevSongs(): Boolean {
        return song > 0
    }

    fun hasMoreSongs(): Boolean {
        return song < songs.size - 1
    }

    fun canJumpTo(songPosition: Int): Boolean {
        return songPosition >= 0 && songPosition < songs.size
    }

    fun goToPrevSong() {
        song -= 1
        seconds = 0.0
    }

    fun goToNextSong() {
        song += 1
        seconds = 0.0
    }

    fun jumpTo(songPosition: Int) {
        song = songPosition
        seconds = 0.0
    }

    fun getCacheStateAsJson(downloading: Pair<String, Double>?, cache: MediaCache): JsonArray<JsonObject> {
        return JsonArray(
            songs.map {
                JsonObject(mapOf(Pair("slug", it.path),
                        Pair("state", if (downloading != null && it.path == downloading.first) {
                            Log.d(TAG, "Updating play state with special download state: ${downloading.second} for ${downloading.first}")
                            downloading.second
                        } else if (cache.has(it.path)) {
                            1.0
                        } else {
                            0.0
                        })
                ))
            }
        )
    }

    companion object {
        fun emptyQueue(placeholderName: String): PlayQueue {
            return PlayQueue(placeholderName, ArrayList(), 0, 0.0)
        }
    }

    fun findAll(slug: String): List<Pair<Int, QueueItem>> {
        return songs.mapIndexedNotNull { i, queueItem ->
            if (queueItem.path == slug) {
                Pair(i, queueItem)
            } else {
                null
            }
        }
    }
}

fun generatePlayQueue(json: JsonObject): PlayQueue {
    val songs: List<QueueItem> = (json.get("queue") as? JsonArray<*>)?.map({
        if (it is JsonObject) {
            QueueItem.fromJson(it)
        } else {
            null
        }
    })?.filterNotNull() ?: ArrayList()
    val song: Int = json.get("song") as? Int ?: 0
    val seconds: Double = json.get("seconds") as? Double ?: (json.get("seconds") as? Int)?.toDouble() ?: 0.0
    val name: String = json.get("id") as? String ?: "Invalid Queue"
    return PlayQueue(name, songs, song, seconds)
}
