package org.guodman.coordinatedmusic.model

import android.content.Context

data class Server(val name: String, val host: String, val token: String) {
    fun save(c: Context) {
        val prefs = c.getSharedPreferences(Factory.SHARED_PREFS_NAME, Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.putString(name, host)
        editor.putString("${name}_token", token)
        editor.apply()
    }

    fun delete(c: Context) {
        val prefs = c.getSharedPreferences(Factory.SHARED_PREFS_NAME, Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.remove(name)
        editor.remove("${name}_token")
        editor.apply()
    }

    companion object Factory {
        val SHARED_PREFS_NAME = "servers"

        fun getAllServers(c: Context): List<Server> {
            val prefs = c.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)
            return prefs.all.map({
                val token = prefs.getString("${it.key}_token", null)
                if (token != null) {
                    Server(it.key, it.value as String, token)
                } else {
                    null
                }
            }).filterNotNull()
        }
    }
}
