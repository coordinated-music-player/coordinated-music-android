package org.guodman.coordinatedmusic.model

import android.util.Log
import com.beust.klaxon.JsonObject
import org.guodman.coordinatedmusic.TAG

class QueueItem(val path: String,
                val album: String,
                val artist: String,
                val secondsDuration: Double,
                val title: String,
                val track: Int,
                val year: Int) {

    var cacheState = 0.0

    fun setCacheState(state: Double): Boolean {
        val isChanged = cacheState != state
        cacheState = state
        return isChanged
    }

    companion object {
        fun fromJson(data: JsonObject): QueueItem? {
            val path: String? = data.get("path") as? String
            return if (path != null) {
                val album = data.get("album") as? String ?: "Unknown Album"
                val artist = data.get("artist") as? String ?: "Unknown Artist"
                val seconds = data.get("secondsDuration") as? Double ?: (data.get("secondsDuration") as? Int)?.toDouble() ?: 0.0
                val maybeTitle = data.get("title") as? String
                val title = if (maybeTitle != null && maybeTitle.isNotEmpty()) { maybeTitle } else { path.split("/").last() }
                val track = data.get("track") as? Int ?: 0
                val year = data.get("year") as? Int ?: 0
                QueueItem(path,
                        album,
                        artist,
                        seconds,
                        title,
                        track,
                        year
                )
            } else {
                null
            }
        }
    }
}
