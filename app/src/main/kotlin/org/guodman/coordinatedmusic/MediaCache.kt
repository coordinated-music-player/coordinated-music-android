package org.guodman.coordinatedmusic

import android.content.Context
import android.util.Log
import com.google.common.hash.Hashing
import com.jakewharton.disklrucache.DiskLruCache
import java.io.File
import java.io.FileDescriptor
import java.io.FileInputStream
import java.io.InputStream
import java.nio.charset.Charset

class MediaCache(context: Context) {
    val CACHE_MAX_MB: Long = 1024
    val CACHE_MAX_BYTES: Long = 1024*1024*CACHE_MAX_MB
    val CACHE_MAX_FILES = 1
    val FILE_INDEX = 0
    val CACHE_DIR = File(context.getCacheDir().absolutePath + "/coordinated-media/")
    val cache: DiskLruCache = DiskLruCache.open(CACHE_DIR, 0, CACHE_MAX_FILES, CACHE_MAX_BYTES)

    fun get(key: String): FileDescriptor? {
        return (cache.get(sha1(key))?.getInputStream(FILE_INDEX) as? FileInputStream)?.fd
    }

    fun put(canceler: DownloadCanceler, key: String, data: InputStream, size: Int?, progress: (complete: Double) -> Unit): Boolean {
        val chunkSize = 1024
        val updateInterval = 1024 * 512
        val editor = cache.edit(sha1(key))
        if (editor == null) { return false }
        val stream = editor.newOutputStream(FILE_INDEX)
        val buffer = ByteArray(chunkSize)
        var copied = 0
        var lastUpdate = 0
        var len = data.read(buffer)
        while (len != -1) {
            if (canceler.isCanceled()) {
                return false
            }
            stream.write(buffer, 0, len)
            copied += len
            if (size != null && size > 0 && copied >= (lastUpdate + updateInterval)) {
                lastUpdate = copied
                progress(copied.toDouble() / size.toDouble())
            }
            len = data.read(buffer)
        }
        stream.flush()
        stream.close()
        editor.commit()
        return true
    }

    fun has(key: String): Boolean {
        return cache.get(sha1(key)) != null
    }

    fun remove(key: String) {
        cache.remove(sha1(key))
    }

    fun sha1(key: String): String {
        return Hashing.sha1().hashString(key, Charset.defaultCharset()).toString()
    }
}
