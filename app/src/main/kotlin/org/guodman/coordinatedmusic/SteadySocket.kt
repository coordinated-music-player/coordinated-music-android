package org.guodman.coordinatedmusic

import android.os.SystemClock
import android.util.Log
import com.neovisionaries.ws.client.*
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.TimeUnit

class SteadySocket(val url: String, val onConnectAction: (SteadySocket) -> Unit) {
    val TAG = "SteadySocket"
    val TERMINATE_MSG = "TERMINATE"

    var ws: WebSocket? = null
    var listener: (String) -> Unit = {
        Log.d(TAG, "No listener connected to the socket yet, but a message has been received")
    }
    var messages: ArrayBlockingQueue<String> = ArrayBlockingQueue(50)
    var shouldStayConnected = true

    init {
        watchQueue()
    }

    fun sendText(msg: String) {
        messages.offer(msg)
    }

    private fun watchQueue() {
        Log.d(TAG, "running watchQueue")
        Thread {
            connect()
            var msg: String? = ""
            while (shouldStayConnected) {
                if (!(ws?.isOpen ?: false)) {
                    connect()
                }
                msg = messages.poll(5, TimeUnit.SECONDS)
                if (msg != null && msg != TERMINATE_MSG) {
                    ws?.sendText(msg)
                }
            }
            Log.d(TAG, "Terminating Queue Watcher")
        }.start()
    }

    private fun connect() {
        Log.d(TAG, "Running connect")
        var connected = false
        while (!connected) {
            Log.d(TAG, "Connecting to websocket: ${url}")
            val ws = WebSocketFactory().createSocket(url)
            ws.addListener(ForwardSocketListener())
            try {
                ws.connect()
                if (ws.isOpen) {
                    onConnectAction(this)
                    this.ws = ws
                    connected = true
                } else {
                    SystemClock.sleep(5000)
                }
            } catch (e: WebSocketException) {
                Log.d(TAG, "Unable to connect, try again")
                SystemClock.sleep(5000)
            }
        }
    }

    fun disconnect() {
        shouldStayConnected = false
        sendText(TERMINATE_MSG)
        ws?.disconnect()
    }

    inner class ForwardSocketListener: WebSocketAdapter() {
        override fun onConnected(websocket: WebSocket?, headers: Map<String, List<String>>) {
            Log.d(TAG, "websocket connected")
        }

        override fun onDisconnected(websocket: WebSocket?, serverCloseFrame: WebSocketFrame, clientCloseFrame: WebSocketFrame, closedByServer: Boolean) {
            Log.d(TAG, "websocket closed")
            if (shouldStayConnected) {
                SystemClock.sleep(3000)
                Log.d(TAG, "Socket is disconnecting.  The loop should reconnect us if it wants us to stay open.")
            }
        }

        override fun onTextFrame(websocket: WebSocket?, frame: WebSocketFrame) {
            if (frame.payloadText != null) {
                listener(frame.payloadText)
            }
        }
    }
}
