package org.guodman.coordinatedmusic

import android.content.Context
import android.support.v4.content.AsyncTaskLoader
import android.util.Log
import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.beust.klaxon.json
import com.squareup.okhttp.*
import org.guodman.coordinatedmusic.model.MediaFolder
import org.guodman.coordinatedmusic.model.QueueItem
import org.guodman.coordinatedmusic.model.generateBaseMediaFolder
import java.net.CookieManager
import java.net.SocketTimeoutException
import java.net.URL
import java.net.URLEncoder

val TAG = "WebAPI"

abstract class CachingAsyncLoader<T>(context: Context): AsyncTaskLoader<T>(context) {
    private var data: T? = null

    override fun onStartLoading() {
        val checkData = data
        if (checkData != null) {
            deliverResult(checkData)
        } else {
            forceLoad()
        }
    }

    override fun deliverResult(newData: T) {
        data = newData
        super.deliverResult(data)
    }
}

fun appendToQueue(context: Context, queueName: String, slugPath: String, callback: () -> Unit, failure: (msg: String) -> Unit) {
    val server = GeneralPrefs.getActiveServer(context)
    Thread(Runnable {
        val base: URL = URL(server.host)
        val builder: HttpUrl.Builder = HttpUrl.Builder()
                .scheme(base.protocol)
                .host(base.host)
                .addQueryParameter("name", queueName)
        if (base.port > 0) {
            builder.port(base.port)
        }

        "api/queue_add/$slugPath".split("/").forEach {
            if (it.isNotEmpty()) {
                builder.addEncodedPathSegment(URLEncoder.encode(it, "UTF-8"))
            }
        }
        val url: HttpUrl = builder.build()
        Log.d(TAG, "Sending append request to ${url}")
        val request = Request.Builder()
                .url(url)
                .put(RequestBody.create(MediaType.parse("application/json"), "{}"))
                .build()
        try {
            val response = client().newCall(request).execute()
            Log.d(TAG, "Response from appending to queue: ${response.code()} :: ${response.body().toString()}")
            callback()
        } catch (e: SocketTimeoutException) {
            failure(e.toString())
        }
    }).start()
}

fun replaceQueue(context: Context, queueName: String, slugPath: String, callback: () -> Unit, failure: (msg: String) -> Unit) {
    val server = GeneralPrefs.getActiveServer(context)
    Thread(Runnable {
        val base: URL = URL(server.host)
        val builder: HttpUrl.Builder = HttpUrl.Builder()
                .scheme(base.protocol)
                .host(base.host)
                .addQueryParameter("name", queueName)
        if (base.port > 0) {
            builder.port(base.port)
        }

        "api/queue_replace/$slugPath".split("/").forEach {
            if (it.isNotEmpty()) {
                builder.addEncodedPathSegment(URLEncoder.encode(it, "UTF-8"))
            }
        }
        val url: HttpUrl = builder.build()
        Log.d(TAG, "Sending replace request to ${url}")
        val request = Request.Builder()
                .url(url)
                .put(RequestBody.create(MediaType.parse("application/json"), "{}"))
                .build()
        try {
            val response = client().newCall(request).execute()
            Log.d(TAG, "Response from appending to queue: ${response.code()} :: ${response.body().toString()}")
            callback()
        } catch (e: SocketTimeoutException) {
            failure(e.toString())
        }
    }).start()
}

fun sendUpdateQueue(context: Context, queueName: String, media: List<QueueItem>) {
    val server = GeneralPrefs.getActiveServer(context)
    Thread(Runnable {
        val url = "${server.host}/api/queue/${URLEncoder.encode(queueName, "UTF-8")}"
        try {
            val paths = json {
                array(media.map { it.path })
            }
            Log.d(TAG, "Sending new play queue: ${paths}")
            val request = Request.Builder()
                    .url(url)
                    .put(RequestBody.create(MediaType.parse("application/json"), paths.toJsonString()))
                    .build()
            val response = client().newCall(request).execute()
            Log.d(TAG, "Response from appending to queue: ${response.code()} :: ${response.body().toString()}")
        } catch (e: SocketTimeoutException) {
            Log.w(TAG, "Socket timed out while updating play queue: ${url}", e)
        }
    }).start()
}

class AgentsLoader(context: Context): CachingAsyncLoader<List<String>>(context) {
    override fun loadInBackground(): List<String>? {
        try {
            val server = GeneralPrefs.getActiveServer(context)
            val request = Request.Builder().url("${server.host}/agent/list").build()
            val response = client().newCall(request).execute()
            val str: String = response.body().string()
            val json = Parser().parse(str.byteInputStream()) as? JsonArray<*>
            return json?.filter({
                it is String
            })?.map({
                it as String
            }) ?: java.util.ArrayList<String>()
        } catch (e: SocketTimeoutException) {
            return null
        }
    }
}

class PlayQueuesLoader(context: Context): CachingAsyncLoader<List<String>>(context) {
    override fun loadInBackground(): List<String>? {
        try {
            val server = GeneralPrefs.getActiveServer(context)
            val request = Request.Builder().url("${server.host}/api/queues").build()
            val response = client().newCall(request).execute()
            val str: String = response.body().string()
            Log.d(TAG, "The json is ${str}")
            val json = Parser().parse(str.byteInputStream()) as? JsonArray<*>
            return json?.filter({
                it is String
            })?.map({
                it as String
            }) ?: java.util.ArrayList<String>()
        } catch (e: SocketTimeoutException) {
            return null
        }
    }
}

fun saveMediaToCache(context: Context, canceler: DownloadCanceler, path: String, cache: MediaCache, retries: Int, progress: (completion: Double) -> Unit): Boolean {
    val server = GeneralPrefs.getActiveServer(context)
    val encoded = path.split("/").map({ URLEncoder.encode(it, "UTF-8") }).joinToString("/")
    val url = "${server.host}/api/fetch/$encoded"
    Log.d(TAG, "Starting download of ${url}")
    val request = Request.Builder().url(url).header("token", server.token).build()
    try {
        val response = client().newCall(request).execute()
        val size: Int? = {
            val base = response.header("Content-Length").toInt()
            if (base > 0) {
                base
            } else {
                null
            }
        }()
        Log.d(TAG, "Completing download of ${url} :: ${response.code()} :: ${response.header("Content-Length")}")
        val result = try {
            val stream = response.body().byteStream()
            cache.put(canceler, path, stream, size, progress)
        } catch (e: SocketTimeoutException) {
            false
        }
        if (response.isSuccessful) {
            Log.d(TAG, "Successfully downloaded ${path}")
            return true
        } else {
            Log.d(TAG, "Failed to download ${path}, removing it from cache")
            cache.remove(path)
            if (retries > 0) {
                return saveMediaToCache(context, canceler, path, cache, retries - 1, progress)
            } else {
                return false
            }
        }
    } catch (e: SocketTimeoutException) {
        if (retries > 0) {
            return saveMediaToCache(context, canceler, path, cache, retries - 1, progress)
        } else {
            return false
        }
    }
}

class DownloadCanceler {
    private var _isCancel = false

    fun cancel() {
        _isCancel = true
    }

    fun isCanceled(): Boolean {
        return _isCancel
    }
}

class MediaListLoader(context: Context, val path: String): AsyncTaskLoader<MediaFolder?>(context) {
    var media: MediaFolder? = null

    override fun onStartLoading() {
        val checkData = media
        if (checkData != null) {
            deliverResult(checkData)
        } else {
            forceLoad()
        }
    }

    override fun deliverResult(newData: MediaFolder?) {
        media = newData
        super.deliverResult(newData)
    }

    override fun loadInBackground(): MediaFolder? {
        try {
            val server = GeneralPrefs.getActiveServer(context)
            val request = Request.Builder().url("${server.host}/api/list/${encodePath(path)}").build()
            val response = client().newCall(request).execute()
            val str: String = response.body().string()
            Log.d(TAG, "json body: ${str}")
            val json = Parser().parse(str.byteInputStream()) as? JsonObject
            return generateBaseMediaFolder(json ?: JsonObject())
        } catch (e: SocketTimeoutException) {
            return null
        }
    }
}

fun sendRegister(host: String, agentName: String): String? {
    val url = "${host}/auth/register/${URLEncoder.encode(agentName, "UTF-8")}"
    Log.d(TAG, "Requesting token: ${url}")
    val request = Request.Builder()
            .url(url)
            .build()
    val response = client().newCall(request).execute()
    val str: String = response.body().string()
    Log.d(TAG, "Response with token: ${str}")
    var token: String? = null
    try {
        val json: JsonObject? = Parser().parse(str.byteInputStream()) as? JsonObject
        token = json?.get("token") as? String
    } catch (e: RuntimeException) {
        Log.d(TAG, "Response is not json")
    }
    Log.d(TAG, "${url} : ${response.code()}")
    return token
}

fun sendPlay(context: Context, agent: String) {
    try {
        val server = GeneralPrefs.getActiveServer(context)
        val url = "${server.host}/agent/play/${URLEncoder.encode(agent, "UTF-8")}"
        val request = Request.Builder()
                .url(url)
                .post(RequestBody.create(MediaType.parse("application/json"), ""))
                .build()
        val response = client().newCall(request).execute()
        Log.d(TAG, "${url} : ${response.code()}")
    } catch (e: SocketTimeoutException) {
    }
}

fun sendPause(context: Context, agent: String) {
    try {
        val server = GeneralPrefs.getActiveServer(context)
        val url = "${server.host}/agent/pause/${URLEncoder.encode(agent, "UTF-8")}"
        val request = Request.Builder()
                .url(url)
                .post(RequestBody.create(MediaType.parse("application/json"), ""))
                .build()
        val response = client().newCall(request).execute()
        Log.d(TAG, "${url} : ${response.code()}")
    } catch (e: SocketTimeoutException) {
    }
}

fun sendNext(context: Context, agent: String) {
    try {
        val server = GeneralPrefs.getActiveServer(context)
        val url = "${server.host}/agent/next/${URLEncoder.encode(agent, "UTF-8")}"
        val request = Request.Builder()
                .url(url)
                .post(RequestBody.create(MediaType.parse("application/json"), ""))
                .build()
        val response = client().newCall(request).execute()
        Log.d(TAG, "${url} : ${response.code()}")
    } catch (e: SocketTimeoutException) {
    }
}

fun sendPrev(context: Context, agent: String) {
    try {
        val server = GeneralPrefs.getActiveServer(context)
        val url = "${server.host}/agent/prev/${URLEncoder.encode(agent, "UTF-8")}"
        val request = Request.Builder()
                .url(url)
                .post(RequestBody.create(MediaType.parse("application/json"), ""))
                .build()
        val response = client().newCall(request).execute()
        Log.d(TAG, "${url} : ${response.code()}")
    } catch (e: SocketTimeoutException) {
    }
}

fun sendJump(context: Context, agent: String, songPosition: Int) {
    try {
        val server = GeneralPrefs.getActiveServer(context)
        val url = "${server.host}/agent/jump/${URLEncoder.encode(agent, "UTF-8")}/${URLEncoder.encode(songPosition.toString(), "UTF-8")}"
        val request = Request.Builder()
                .url(url)
                .post(RequestBody.create(MediaType.parse("application/json"), ""))
                .build()
        val response = client().newCall(request).execute()
        Log.d(TAG, "${url} : ${response.code()}")
    } catch (e: SocketTimeoutException) {
    }
}

fun sendSeek(context: Context, agent: String, seconds: Int) {
    try {
        val server = GeneralPrefs.getActiveServer(context)
        val url = "${server.host}/agent/seek/${URLEncoder.encode(agent, "UTF-8")}/${URLEncoder.encode(seconds.toString(), "UTF-8")}"
        val request = Request.Builder()
                .url(url)
                .post(RequestBody.create(MediaType.parse("application/json"), ""))
                .build()
        val response = client().newCall(request).execute()
        Log.d(TAG, "${url} : ${response.code()}")
    } catch (e: SocketTimeoutException) {
    }
}

fun sendPlayQueue(context: Context, agent: String, queueName: String) {
    try {
        val server = GeneralPrefs.getActiveServer(context)
        val url = "${server.host}/agent/queue/${agent}/${queueName}"
        val request = Request.Builder()
                .url(url)
                .post(RequestBody.create(MediaType.parse("application/json"), ""))
                .build()
        val response = client().newCall(request).execute()
        Log.d(TAG, "${url} : ${response.code()}")
    } catch (e: SocketTimeoutException) {
    }
}

fun sendDeleteQueue(context: Context, queueName: String) {
    try {
        val server = GeneralPrefs.getActiveServer(context)
        val url = "${server.host}/api/queue/${URLEncoder.encode(queueName, "UTF-8")}"
        val request = Request.Builder()
                .url(url)
                .delete()
                .build()
        val response = client().newCall(request).execute()
        Log.d(TAG, "${url} : ${response.code()}")
    } catch (e: SocketTimeoutException) {
    }
}

fun encodePath(raw: String): String {
    val pieces = raw.split("/").map {
        URLEncoder.encode(it, "UTF-8")
    }
    return pieces.joinToString("/")
}

val LOADER_AGENTS = 0
val LOADER_QUEUES = 1
val LOADER_MEDIA = 2

var _client: OkHttpClient? = null
private fun client(): OkHttpClient {
    if (_client == null) {
        _client = OkHttpClient()
        val cookieManager = CookieManager()
        cookieManager.setCookiePolicy(java.net.CookiePolicy.ACCEPT_ALL)
        _client?.cookieHandler = cookieManager
    }
    return _client!!
}
