package org.guodman.coordinatedmusic

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.MotionEventCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_edit_queue.*
import org.guodman.coordinatedmusic.model.PlayQueue
import org.guodman.coordinatedmusic.model.QueueItem
import java.util.*

class EditQueueActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_queue)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        fabDelete.setOnClickListener {
            Log.d(TAG, "Should we delete?")
            val name = EditQueueActivity.queueName
            if (name != null) {
                Thread {
                    Log.d(TAG, "Deleting Queue")
                    sendDeleteQueue(this, name)
                    finish()
                }.start()
            }
        }

        fabSave.setOnClickListener {
            val name = EditQueueActivity.queueName
            if (name != null) {
                sendUpdateQueue(this, name, EditQueueActivity.queue)
                finish()
            }
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (queueName == null) {
            finish()
        }

        songsView.setHasFixedSize(true)
        songsView.layoutManager = LinearLayoutManager(this)
        val adapter = PlayQueueAdapter()
        songsView.adapter = adapter
    }

    inner class PlayQueueAdapter(): RecyclerView.Adapter<PlayQueueAdapter.PlayQueueViewHolder>() {
        val touchHelper = TouchHelper()
        val itemTouchHelper = ItemTouchHelper(touchHelper)

        init {
            itemTouchHelper.attachToRecyclerView(songsView)
        }

        override fun getItemCount(): Int {
            return EditQueueActivity.queue.size
        }

        override fun onBindViewHolder(holder: PlayQueueViewHolder?, position: Int) {
            holder?.setData(EditQueueActivity.queue[position], position, false)
            holder?.handleView?.setOnTouchListener(object : View.OnTouchListener {
                override fun onTouch(v: View, event: MotionEvent): Boolean {
                    if (MotionEventCompat.getActionMasked(event) === MotionEvent.ACTION_DOWN) {
                        itemTouchHelper.startDrag(holder)
                    }
                    return false
                }
            })
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PlayQueueViewHolder {
            val view = LayoutInflater.from(parent?.context).inflate(R.layout.card_edit_queue, parent, false) as RelativeLayout
            return PlayQueueViewHolder(view)
        }

        inner class TouchHelper: ItemTouchHelper.Callback() {
            override fun getMovementFlags(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?): Int {
                val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
                val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
                return makeMovementFlags(dragFlags, swipeFlags)
            }

            override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?): Boolean {
                val fromPosition: Int? = viewHolder?.getAdapterPosition()
                val toPosition: Int? = target?.getAdapterPosition()
                if (fromPosition != null && toPosition != null) {
                    if (fromPosition < toPosition) {
                        for (i in fromPosition..toPosition - 1) {
                            Collections.swap(EditQueueActivity.queue, i, i + 1)
                        }
                    } else {
                        for (i in fromPosition downTo toPosition + 1) {
                            Collections.swap(EditQueueActivity.queue, i, i - 1)
                        }
                    }
                    notifyItemMoved(fromPosition, toPosition)
                }
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
                val position = viewHolder?.getAdapterPosition()
                if (position != null) {
                    EditQueueActivity.queue = EditQueueActivity.queue.filterIndexed { i, queueItem -> i != position }
                    notifyItemRemoved(position)
                }
            }
        }

        inner class PlayQueueViewHolder(val view: ViewGroup): RecyclerView.ViewHolder(view) {
            var songName: String? = null
            var songPosition: Int? = null
            var isCurrent: Boolean = false
            val handleView = itemView.findViewById(R.id.handle) as? ImageView

            fun setData(name: QueueItem, position: Int, isCurrent: Boolean) {
                songName = name.path
                songPosition = position
                this.isCurrent = isCurrent

                val title = view.findViewById(R.id.songName) as TextView
                val album = view.findViewById(R.id.album) as TextView
                val artist = view.findViewById(R.id.artist) as TextView
                album.text = "${name.album}${ if (name.track > 0) { " ${name.track}"} else {""} }"
                artist.text = name.artist
                title.text = name.title
            }

            init {
                view.setOnClickListener {}

                view.setOnLongClickListener {false}
            }
        }
    }

    companion object {
        var queueName: String? = null
        var queue: List<QueueItem> = ArrayList()
    }
}

fun createEditQueueIntent(context: Context, queue: PlayQueue): Intent {
    EditQueueActivity.queue = queue.songs.filter { true }
    Log.d(TAG, "songs added to queue: ${EditQueueActivity.queue.size}")
    EditQueueActivity.queueName = queue.name
    val i = Intent(context, EditQueueActivity::class.java)
    return i
}