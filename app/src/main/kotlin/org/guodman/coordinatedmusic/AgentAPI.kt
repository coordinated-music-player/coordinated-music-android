package org.guodman.coordinatedmusic

import com.beust.klaxon.JsonObject
import com.beust.klaxon.json
import org.guodman.coordinatedmusic.model.PlayQueue

object AgentAPI {
    val COMMAND_QUEUE_DROPPED: JsonObject = json {
        obj(Pair("command", "queue_dropped"))
    }

    fun COMMAND_POSITION(seconds: Double, song: Int, queueName: String): JsonObject {
        return json {
            obj(
                    Pair("command", "position"),
                    Pair("seconds", seconds),
                    Pair("song", song),
                    Pair("queue", queueName))
        }
    }

    fun COMMAND_UPDATE_PLAY_STATE(playState: PlayState, seconds: Double, downloading: Pair<String, Double>?, queue: PlayQueue?, cache: MediaCache): JsonObject {
        return json {
            obj(Pair("command", "update_state"),
                    Pair("playState", playState.toPrintableString()),
                    Pair("seconds", seconds),
                    Pair("cache", queue?.getCacheStateAsJson(downloading, cache))
            )
        }
    }

    fun COMMAND_INFO(msg: String): JsonObject {
        return json {
            obj(Pair("command", "info"),
                    Pair("msg", msg)
            )
        }
    }

    enum class PlayState(val s: String) {
        PLAY("play"),
        PAUSE("pause"),
        STOP("stop");

        fun toPrintableString(): String {
            return s
        }
    }

    fun getPlayStateFromString(s: String?): PlayState? {
        return when(s) {
            "play" -> { PlayState.PLAY }
            "pause" -> { PlayState.PAUSE }
            "stop" -> { PlayState.STOP }
            else -> { null }
        }
    }
}
