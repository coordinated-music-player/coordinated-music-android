package org.guodman.coordinatedmusic

import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_server_list.*
import org.guodman.coordinatedmusic.model.Server

class ServerListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_server_list)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        server_list.setHasFixedSize(true)
        server_list.layoutManager = LinearLayoutManager(this)
        val serversAdapter = ServerListAdapter(Server.Factory.getAllServers(this))
        server_list.adapter = serversAdapter

        val fab = findViewById(R.id.fab) as FloatingActionButton
        fab.setOnClickListener { view ->
            val dialog: Dialog = Dialog(this)
            dialog.setContentView(R.layout.dialog_add_server)
            val cancel = dialog.findViewById(R.id.cancel) as Button
            val add = dialog.findViewById(R.id.add_button) as Button
            val name = dialog.findViewById(R.id.name) as TextView
            val host = dialog.findViewById(R.id.host) as TextView

            cancel.setOnClickListener { dialog.dismiss() }

            add.setOnClickListener {
                Thread {
                    val hostName = host.text.toString()
                    val agentName = name.text.toString()
                    Log.d(TAG, "register button clicked with '${hostName}' and '${agentName}'")
                    if (hostName != "" && agentName != "") {
                        val token = sendRegister(hostName, agentName)
                        if (token != null) {
                            val server = Server(name.text.toString(), host.text.toString(), token)
                            server.save(this)
                            serversAdapter.add(server)
                            runOnUiThread {
                                dialog.dismiss()
                            }
                        } else {
                            runOnUiThread {
                                Toast.makeText(this@ServerListActivity, "Error connecting to this server", Toast.LENGTH_LONG)
                            }
                        }
                    }
                }.start()
            }

            dialog.show()
            val window = dialog.window
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        }
    }
}

class ServerListAdapter(var servers: List<Server>): RecyclerView.Adapter<ServerListAdapter.ServerListViewHolder>() {
    fun add(server: Server) {
        servers += server
        notifyItemInserted(servers.size - 1)
    }

    fun remove(server: Server) {
        val position: Int = servers.indexOf(server)
        servers -= server
        notifyItemRemoved(position)
    }

    override fun onBindViewHolder(holder: ServerListViewHolder?, position: Int) {
        holder?.server = servers[position]
    }

    override fun getItemCount(): Int {
        return servers.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ServerListViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.card_server_item, parent, false) as RelativeLayout
        return ServerListViewHolder(view)
    }

    inner class ServerListViewHolder(val view: ViewGroup): RecyclerView.ViewHolder(view) {
        var server: Server? = null
            set(value) {
                val nameView = view.findViewById(R.id.server_name) as TextView
                nameView.text = value?.name
                val hostView = view.findViewById(R.id.server_host) as TextView
                hostView.text = value?.host
                field = value
            }

        init {
            view.setOnClickListener {
                val s = this.server
                if (s != null) {
                    GeneralPrefs.saveActiveServer(view.context, s)
                    val intent = createMainActivityIntent(view.context)
                    view.context.startActivity(intent)
                }
            }
            view.setOnLongClickListener {
                val s = this.server
                if (s != null) {
                    s.delete(view.context)
                    this@ServerListAdapter.remove(s)
                }
                true
            }
        }
    }
}
