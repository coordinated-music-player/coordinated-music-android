package org.guodman.coordinatedmusic

import com.beust.klaxon.JsonObject
import com.beust.klaxon.json

object InterfaceAPI {
    fun COMMAND_REGISTER(name: String): JsonObject {
        return json {
            obj(Pair("command", "register"), Pair("name", name))
        }
    }

    fun COMMAND_LISTEN_TO_AGENT(agent: String): JsonObject {
        return json {
            obj(Pair("command", "listen"), Pair("agent", agent))
        }
    }
}
