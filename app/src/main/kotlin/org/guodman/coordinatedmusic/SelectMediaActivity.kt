package org.guodman.coordinatedmusic

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_select_media.*
import org.guodman.coordinatedmusic.model.MediaFile
import org.guodman.coordinatedmusic.model.MediaFolder
import java.util.*

class SelectMediaActivity: AppCompatActivity() {
    val TAG = "SelectMediaActivity"
    var media: MediaFolder = MediaFolder("", "", ArrayList<MediaFolder>(), ArrayList<MediaFile>())
    var mediaPath: ArrayList<String> = ArrayList()
    var queueName: String = "default"
    var queues: List<String> = ArrayList<String>()

    private val queuesCallback: LoaderManager.LoaderCallbacks<List<String>> = object: LoaderManager.LoaderCallbacks<List<String>> {
        override fun onCreateLoader(id: Int, args: Bundle?): Loader<List<String>>? {
            return PlayQueuesLoader(this@SelectMediaActivity)
        }

        override fun onLoadFinished(loader: Loader<List<String>>?, data: List<String>?) {
            Log.d(TAG, "Queues Loaded: ${data}")
            queues = data ?: ArrayList()
        }

        override fun onLoaderReset(loader: Loader<List<String>>?) {
            queues = ArrayList()
        }
    }

    private val mediaCallback: LoaderManager.LoaderCallbacks<MediaFolder> = object: LoaderManager.LoaderCallbacks<MediaFolder> {
        override fun onCreateLoader(id: Int, args: Bundle?): Loader<MediaFolder?>? {
            return MediaListLoader(this@SelectMediaActivity, args?.getString("path") ?: "")
        }

        override fun onLoadFinished(loader: Loader<MediaFolder>?, data: MediaFolder?) {
            Log.d(TAG, "Media loaded: ${data}")
            val tmpMedia = data ?: MediaFolder("", "", ArrayList<MediaFolder>(), ArrayList<MediaFile>())
            media = findMediaFolder(tmpMedia, mediaPath)
            mediaList.adapter = MediaListAdapter(media, queueName, mediaPath)
            media.children.forEach {
                Log.d(TAG, it.path)
            }
        }

        override fun onLoaderReset(loader: Loader<MediaFolder>?) {
            val tmpMedia = MediaFolder("", "", ArrayList<MediaFolder>(), ArrayList<MediaFile>())
            media = findMediaFolder(tmpMedia, mediaPath)
            mediaList.adapter = MediaListAdapter(media, queueName, mediaPath)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_media)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mediaPath = intent?.getStringArrayListExtra("mediaPath") ?: ArrayList<String>()
        val printableMediaPath = mediaPath.fold("ROOT") { a, b -> "${a}, ${b}" }
        Log.d(TAG, "Media path is now: ${printableMediaPath}")

        queueName = intent?.getStringExtra("queueName") ?: "default"

        mediaList.setHasFixedSize(true)
        mediaList.layoutManager = LinearLayoutManager(this)
        mediaList.adapter = MediaListAdapter(media, queueName, mediaPath)

        val bundle: Bundle = Bundle()
        bundle.putString("path", mediaPath.joinToString("/"))
        supportLoaderManager.initLoader(LOADER_MEDIA, bundle, mediaCallback)
    }

    override fun onResume() {
        super.onResume()
        supportLoaderManager.restartLoader(LOADER_QUEUES, null, queuesCallback)
    }

    fun findMediaFolder(root: MediaFolder, path: ArrayList<String>): MediaFolder {
        return path.fold(root) { folder, nextPath ->
            folder.children.find { it.path == nextPath } ?: folder
        }
    }

    inner class MediaListAdapter(val media: MediaFolder, val queueName: String, val mediaPath: ArrayList<String>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        val TYPE_FOLDER = 1
        val TYPE_FILE = 2

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
            if (getItemViewType(position) == TYPE_FOLDER) {
                (holder as? MediaListViewHolder)?.mediaFolder = media.children[position]
            } else {
                (holder as? MediaSongViewHolder)?.mediaFile = media.media[position - media.children.size]
            }
        }

        override fun getItemViewType(position: Int): Int {
            if (position < media.children.size) {
                return TYPE_FOLDER
            } else {
                return TYPE_FILE
            }
        }

        override fun getItemCount(): Int {
            return media.children.size + media.media.size
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder? {
            return if (viewType == TYPE_FOLDER) {
                val view = LayoutInflater.from(parent?.context).inflate(R.layout.card_media_folder, parent, false) as RelativeLayout
                MediaListViewHolder(view, queueName, mediaPath)
            } else {
                val view = LayoutInflater.from(parent?.context).inflate(R.layout.card_media_folder, parent, false) as RelativeLayout
                MediaSongViewHolder(view, queueName)
            }
        }
    }

    fun addWithDialog(toAdd: String, title: String) {
        startActivity(AddMediaActivity.createIntent(this, toAdd, title))
    }

    inner class MediaSongViewHolder(val view: ViewGroup, val queueName: String): RecyclerView.ViewHolder(view) {
        var mediaFile: MediaFile? = null
            set(value) {
                val v = view.findViewById(R.id.mediaFolderName) as TextView
                val tmp = value
                if (tmp != null && tmp.track > 0) {
                    v.text = "${tmp.track} - ${tmp.title}"
                } else if (tmp != null) {
                    v.text = tmp.title
                } else {
                    v.text = "Not Set"
                }
                field = value
            }

        init {
            view.setOnClickListener {
                val mf = mediaFile
                if (mf != null) {
                    appendToQueue(view.context, queueName, mf.fullPath, {}, { msg ->
                        Toast.makeText(this@SelectMediaActivity, msg, Toast.LENGTH_LONG).show()
                    })
                }
            }
            view.setOnLongClickListener {
                val mf = mediaFile
                if (mf != null) {
                    addWithDialog(mf.fullPath, mf.title)
                }
                true
            }
        }
    }

    inner class MediaListViewHolder(val view: ViewGroup, val queueName: String, val mediaPath: ArrayList<String>): RecyclerView.ViewHolder(view) {
        var mediaFolder: MediaFolder? = null
            set(value) {
                val v = view.findViewById(R.id.mediaFolderName) as TextView
                v.text = value?.path ?: "Not Set"
                field = value
            }

        init {
            view.setOnClickListener {
                val mf = mediaFolder
                if (mf != null) {
                    val v = view.findViewById(R.id.mediaFolderName) as TextView
                    val path: ArrayList<String> = ArrayList()
                    mediaPath.forEach {
                        path.add(it)
                    }
                    path.add(mf.path)
                    val intent = createSelectMediaIntent(v.context, queueName, path)
                    v.context.startActivity(intent)
                }
            }
            view.setOnLongClickListener {
                val mf = mediaFolder
                if (mf != null) {
                    addWithDialog(mf.slug, mf.path)
                }
                true
            }
        }
    }
}

fun createSelectMediaIntent(c: Context, queueName: String, mediaPath: ArrayList<String>): Intent {
    val i = Intent(c, SelectMediaActivity::class.java)
    i.putStringArrayListExtra("mediaPath", mediaPath)
    i.putExtra("queueName", queueName)
    return i
}
