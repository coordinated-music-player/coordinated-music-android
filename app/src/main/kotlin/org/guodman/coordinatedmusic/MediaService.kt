package org.guodman.coordinatedmusic

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.IBinder
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import org.guodman.coordinatedmusic.model.PlayQueue
import org.guodman.coordinatedmusic.model.generatePlayQueue
import java.io.IOException
import java.net.URLEncoder

class MediaService : Service(), MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener {
    companion object {
        val TAG = "MediaService"
        val ACTION_LOAD = "org.guodman.coordinatedmusic.LOAD"
        val ACTION_SET_QUEUE = "org.guodman.coordinatedmusic.SET_QUEUE"
        val ACTION_PLAY = "org.guodman.coordinatedmusic.PLAY"
        val ACTION_STOP = "org.guodman.coordinatedmusic.STOP"
        val ACTION_PAUSE = "org.guodman.coordinatedmusic.PAUSE"
        val ACTION_DISCONNECT = "org.guodman.coordinatedmusic.DISCONNECT"
        val ACTION_PREV = "org.guodman.coordinatedmusic.ACTION_PREV"
        val ACTION_NEXT = "org.guodman.coordinatedmusic.ACTION_NEXT"
        val ACTION_JUMP = "org.guodman.coordinatedmusic.ACTION_JUMP"
        val ACTION_SEEK = "org.guodman.coordinatedmusic.ACTION_SEEK"
        val ACTION_DROP_QUEUE = "org.guodman.coordinatedmusic.ACTION_DROP_QUEUE"

        var passingQueue: PlayQueue? = null

        fun load(c: Context) {
            Log.d(TAG, "running load")
            val i = Intent(c, MediaService::class.java)
            i.action = ACTION_LOAD
            c.startService(i)
        }

        fun setQueue(c: Context, queue: PlayQueue) {
            Log.d(TAG, "running setQueue: ${queue.name}")
            passingQueue = queue // In case the queue is more that 1MB
            val i = Intent(c, MediaService::class.java)
            i.action = ACTION_SET_QUEUE
            c.startService(i)
        }

        fun play(c: Context) {
            Log.d(TAG, "running play")
            val i = Intent(c, MediaService::class.java)
            i.action = ACTION_PLAY
            c.startService(i)
        }

        fun stop(c: Context) {
            Log.d(TAG, "running stop")
            val i = Intent(c, MediaService::class.java)
            i.action = ACTION_STOP
            c.startService(i)
        }

        fun pause(c: Context) {
            Log.d(TAG, "running pause")
            val i = Intent(c, MediaService::class.java)
            i.action = ACTION_PAUSE
            c.startService(i)
        }

        fun prev(c: Context) {
            Log.d(TAG, "running prev")
            val i = Intent(c, MediaService::class.java)
            i.action = ACTION_PREV
            c.startService(i)
        }

        fun next(c: Context) {
            Log.d(TAG, "running next")
            val i = Intent(c, MediaService::class.java)
            i.action = ACTION_NEXT
            c.startService(i)
        }

        fun jump(c: Context, songPosition: Int) {
            Log.d(TAG, "running jump")
            val i = Intent(c, MediaService::class.java)
            i.putExtra("songPosition", songPosition)
            i.action = ACTION_JUMP
            c.startService(i)
        }

        fun seek(c: Context, seconds: Double) {
            Log.d(TAG, "running seek to ${seconds}")
            val i = Intent(c, MediaService::class.java)
            i.action = ACTION_SEEK
            i.putExtra("seconds", seconds)
            c.startService(i)
        }

        fun dropQueue(c: Context) {
            Log.d(TAG, "running drop queue")
            val i = Intent(c, MediaService::class.java)
            i.action = ACTION_DROP_QUEUE
            c.startService(i)
        }

        fun disconnect(c: Context) {
            Log.d(TAG, "running disconnect")
            val i = Intent(c, MediaService::class.java)
            i.action = ACTION_DISCONNECT
            c.startService(i)
        }
    }
    var manager: MediaPlayerManager? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand")
        Log.d(TAG, "intent info: ${intent?.action}")
        if (intent?.action?.equals(ACTION_LOAD) ?: false) {
            val notificationIntent = Intent(this, MainActivity::class.java);
            val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
            val notification: Notification = Notification.Builder(this)
                    .setContentIntent(pendingIntent)
                    .setContentTitle("Coordinated Music Player")
                    .setSmallIcon(R.drawable.notification_template_icon_bg)
                    .build()
            val notificationManager: NotificationManager = getSystemService(AppCompatActivity.NOTIFICATION_SERVICE) as NotificationManager

            this.startForeground(152, notification)
            Thread(Runnable {
                manager = MediaPlayerManager()
            }).start()
        } else if (intent?.action?.equals(ACTION_SET_QUEUE) ?: false) {
            // Assume that passingQueue has already been set
            val q = passingQueue
            if (q != null) {
                manager?.load(q)
            }
        } else if (intent?.action?.equals(ACTION_PLAY) ?: false) {
            Log.d(TAG, "MediaService play")
            manager?.play()
        } else if (intent?.action?.equals(ACTION_STOP) ?: false) {
            Log.d(TAG, "MediaService stop")
            manager?.stop()
        } else if (intent?.action?.equals(ACTION_PAUSE) ?: false) {
            Log.d(TAG, "MediaService pause")
            manager?.pause()
        } else if (intent?.action?.equals(ACTION_PREV) ?: false) {
            manager?.prev()
        } else if (intent?.action?.equals(ACTION_NEXT) ?: false) {
            manager?.next()
        } else if (intent?.action?.equals(ACTION_JUMP) ?: false) {
            val songPosition = intent?.getIntExtra("songPosition", 0) ?: 0
            manager?.jump(songPosition)
        } else if (intent?.action?.equals(ACTION_SEEK) ?: false) {
            val seconds = intent?.getDoubleExtra("seconds", 0.0) ?: 0.0
            Log.d(TAG, "We should seek to ${seconds}")
            manager?.seek(seconds)
        } else if (intent?.action?.equals(ACTION_DROP_QUEUE) ?: false) {
            manager?.dropQueue()
        } else if (intent?.action?.equals(ACTION_DISCONNECT) ?: false) {
            manager?.stop()
            manager?.disconnect()
            stopSelf()
        }
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        Log.d(TAG, "onBind")
        throw UnsupportedOperationException()
    }

    override fun onPrepared(mp: MediaPlayer?) {
        Log.d(TAG, "onPrepared")
        throw UnsupportedOperationException()
    }

    override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
        Log.d(TAG, "onError")
        throw UnsupportedOperationException()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy for the service")
        manager?.kill()
    }

    inner class MediaPlayerManager() {
        val mediaPlayer: MediaPlayer = MediaPlayer()
        val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        var isMediaPlayerPrepared = false
        var queue: PlayQueue? = null
        var playerState = PlayerState.NOT_INITIALIZED
        val cache = MediaCache(this@MediaService)
        val server = GeneralPrefs.getActiveServer(this@MediaService)
        init {
            Log.d(TAG, "Connecting using token ${server.token} encoded as ${URLEncoder.encode(server.token, "UTF-8")}")
        }
        val ws: SteadySocket = SteadySocket("${server.host}/agent/connect/${URLEncoder.encode(server.token, "UTF-8")}", { me ->
            val q = queue
            if (q != null && isMediaPlayerPrepared) {
                me.sendText(AgentAPI.COMMAND_UPDATE_PLAY_STATE(playerStateToPlayState(playerState), getMediaPositionSeconds(), null, q, cache).toJsonString())
            }
        })
        var shouldStayConnected = true
        var isKilled = false
        val downloadCanceler = DownloadCanceler()
        val noisyStreamReceiver = NoisyAudioStreamReceiver()

        fun load(newQueue: PlayQueue) {
            val oldQueue = queue
            Log.d(TAG, "Play queue is ${queue?.name} :: ${queue?.songs?.size}")
            if ((oldQueue != null && newQueue.name != oldQueue.name) || oldQueue == null) {
                val currentIsPlaying = playerState == PlayerState.PLAYING
                mediaPlayer.setOnCompletionListener {
                    Log.d(TAG, "Playback finished, but we are changing the queue, so don't go to the next song")
                }
                stop()
                queue = newQueue
                prepPlayer(currentIsPlaying)
                mediaPlayer.seekTo(queue?.seconds?.toInt() ?: 0)
            } else {
                Log.d(TAG, "No need to update the playlist")
                queue = newQueue
            }
        }

        fun play() {
            if (queue != null) {
                if (!isMediaPlayerPrepared) {
                    prepPlayer(true)
                }
                else {
                    focusAndPlay()
                }
            }
        }

        fun focusAndPlay() {
            if (isMediaPlayerPrepared && !isKilled) {
                val focusResult = audioManager.requestAudioFocus(audioFocusListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN)
                if (focusResult == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    Log.d(TAG, "Audio Focus Gained")
                    noisyStreamReceiver.register()
                    mediaPlayer.start()
                    playerState = PlayerState.PLAYING
                    sendPlayStateUpdate()
                }
            }
        }

        fun pause() {
            noisyStreamReceiver.unregister()
            mediaPlayer.pause()
            audioManager.abandonAudioFocus(audioFocusListener)
            playerState = PlayerState.PAUSED
            sendUpdatedQueuePosition()
            sendPlayStateUpdate()
        }

        fun stop() {
            noisyStreamReceiver.unregister()
            mediaPlayer.stop()
            audioManager.abandonAudioFocus(audioFocusListener)
            playerState = PlayerState.STOPPED
            sendUpdatedQueuePosition()
            sendPlayStateUpdate()
        }

        fun prev() {
            val q = queue
            if (q != null && q.hasPrevSongs()) {
                q.goToPrevSong()
                prepPlayer()
                sendUpdatedQueuePosition()
            }
        }

        fun next() {
            Log.d(TAG, "Going to next song")
            val q = queue
            if (q != null && q.hasMoreSongs()) {
                q.goToNextSong()
                prepPlayer()
                sendUpdatedQueuePosition()
            }
        }

        fun jump(songPosition: Int) {
            val q = queue
            if (q != null && q.canJumpTo(songPosition)) {
                Log.d(TAG, "Doing a real jump to ${songPosition}")
                q.jumpTo(songPosition)
                prepPlayer(true)
                sendUpdatedQueuePosition()
            }
        }

        fun prepPlayer(startPlaying: Boolean = false) {
            val q = queue
            if (q != null) {
                Thread {
                    synchronized(mediaPlayer) {
                        val path = q.getCurrentSongPath()
                        if (path != null) {
                            Log.d(TAG, "prepPlayer")
                            if (!cache.has(path)) {
                                Log.d(TAG, "not in cache, download the file ${path}")
                                val result = saveMediaToCache(this@MediaService, downloadCanceler, path, cache, 2, {progress ->
                                    Log.d(TAG, "Download Progress: ${progress}")
                                    sendPlayStateUpdate(Pair(path, progress))
                                })
                                sendPlayStateUpdate()
                                if (!result) { return@synchronized }
                            }
                            precache(q.getNextSongPath())
                            Log.d(TAG, "Preparing to play ${path}")
                            mediaPlayer.setOnCompletionListener {
                                Log.d(TAG, "Playback finished, but we are changing the queue, so don't go to the next song")
                            }
                            mediaPlayer.setOnSeekCompleteListener {
                                sendPlayStateUpdate()
                            }
                            mediaPlayer.reset()
                            try {
                                val stream = cache.get(path)
                                if (stream == null) {
                                    ws.sendText(AgentAPI.COMMAND_INFO("We thought we had our item in the cache, but we weren't able to get it out").toJsonString())
                                    return@synchronized
                                }
                                mediaPlayer.setDataSource(stream)
                                mediaPlayer.prepare()
                                mediaPlayer.seekTo(q.getMilliseconds())
                                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC)

                                mediaPlayer.setOnCompletionListener {
                                    if (q.hasMoreSongs()) {
                                        Log.d(TAG, "Song finished, trying the next song")
                                        next()
                                    } else {
                                        Log.d(TAG, "Song finished, end of playlist reached")
                                        stop()
                                    }
                                }
                                isMediaPlayerPrepared = true
                                if (startPlaying || playerState == PlayerState.PLAYING) {
                                    focusAndPlay()
                                } else if (playerState == PlayerState.NOT_INITIALIZED) {
                                    playerState = PlayerState.STOPPED
                                }
                            } catch (e: IOException) {
                                mediaPlayer.reset()
                                isMediaPlayerPrepared = false
                                sendPlayStateUpdate()
                                ws.sendText(AgentAPI.COMMAND_INFO("Unable to play song due to IO error while loading from cache").toJsonString())
                                cache.remove(path)
                            }
                        }
                    }
                }.start()
            } else {
                Log.d(TAG, "Not doing prepPlayer stuff")
            }
        }

        fun seek(seconds: Double) {
            Log.d(TAG, "Seeking to ${(seconds * 1000.0).toInt()}")
            mediaPlayer.seekTo((seconds * 1000.0).toInt())
        }

        fun dropQueue() {
            stop()
            queue = null
            ws.sendText(AgentAPI.COMMAND_QUEUE_DROPPED.toJsonString())
        }

        fun sendPlayStateUpdate(downloading: Pair<String, Double>? = null) {
            ws.sendText(AgentAPI.COMMAND_UPDATE_PLAY_STATE(playerStateToPlayState(playerState), getMediaPositionSeconds(), downloading, queue, cache).toJsonString())
        }

        fun sendUpdatedQueuePosition() {
            val q = queue
            if (q != null) {
                ws.sendText(AgentAPI.COMMAND_POSITION(getMediaPositionSeconds(), q.song, q.name).toJsonString())
            }
        }

        fun getMediaPositionSeconds(): Double {
            return mediaPlayer.currentPosition.toDouble() / 1000.0
        }

        fun precache(path: String?) {
            if (path != null && !cache.has(path)) {
                Thread {
                    Log.d(TAG, "not in cache, pre-download the file: ${path}")
                    saveMediaToCache(this@MediaService, downloadCanceler, path, cache, 2, {progress ->
                        Log.d(TAG, "Download Progress: ${progress}")
                        sendPlayStateUpdate(Pair(path, progress))
                    })
                    sendPlayStateUpdate()
                }.start()
            }
        }

        fun kill() {
            downloadCanceler.cancel()
            if (playerState == PlayerState.PLAYING) { stop() }
            disconnect()
            isKilled = true
        }

        fun disconnect() {
            shouldStayConnected = false
            ws.disconnect()
        }

        val audioFocusListener: AudioManager.OnAudioFocusChangeListener = AudioManager.OnAudioFocusChangeListener { focusChange ->
            Log.d(TAG, "Received audio focus change event: ${focusChange}")
            when (focusChange) {
                AudioManager.AUDIOFOCUS_GAIN -> {
                    Log.d(TAG, "AUDIOFOCUS_GAIN")
                    if (!isKilled) {
                        mediaPlayer.setVolume(1f, 1f)
                        mediaPlayer.start()
                        playerState = PlayerState.PLAYING
                        sendPlayStateUpdate()
                    }
                }
                AudioManager.AUDIOFOCUS_LOSS -> {
                    Log.d(TAG, "AUDIOFOCUS_LOSS")
                    pause()
                }
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                    Log.d(TAG, "AUDIOFOCUS_LOSS_TRANSIENT")
                    pause()
                }
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> {
                    Log.d(TAG, "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK")
                    mediaPlayer.setVolume(0.1f, 0.1f)
                }
                else -> {
                    Log.d(TAG, "AUDIOFOCUS ${focusChange}")
                }
            }
        }

        init {
            Thread {
                while (!downloadCanceler.isCanceled()) {
                    Thread.sleep(60000)
                    if (playerState == PlayerState.PLAYING) {
                        sendUpdatedQueuePosition()
                    }
                }
            }.start()

            ws.listener = { payload ->
                val cmd: JsonObject? = Parser().parse(payload.byteInputStream()) as? JsonObject
                Log.d(TAG, "Command: ${cmd?.get("command")}")
                when (cmd?.get("command")) {
                    "giveUp" -> {
                        kill()
                    }
                    "play" -> {
                        Log.d(TAG, "Play Command: '${cmd?.toJsonString()}'")
                        MediaService.play(this@MediaService)
                    }
                    "stop" -> {
                        Log.d(TAG, "Stop command: '${cmd?.toJsonString()}'")
                        MediaService.stop(this@MediaService)
                    }
                    "pause" -> {
                        Log.d(TAG, "Pause command: '${cmd?.toJsonString()}'")
                        MediaService.pause(this@MediaService)
                    }
                    "next" -> {
                        Log.d(TAG, "Next command: '${cmd?.toJsonString()}'")
                        MediaService.next(this@MediaService)
                    }
                    "prev" -> {
                        Log.d(TAG, "Prev command: '${cmd?.toJsonString()}'")
                        MediaService.prev(this@MediaService)
                    }
                    "jump" -> {
                        Log.d(TAG, "Jump command: '${cmd?.toJsonString()}'")
                        val songPosition = cmd?.get("songPosition") as? Int
                        if (songPosition != null) {
                            MediaService.jump(this@MediaService, songPosition)
                        } else {
                            Log.w(TAG, "Received a request to jump to a song, but no song position was given: ${cmd?.toJsonString()}")
                        }
                    }
                    "seek" -> {
                        Log.d(TAG, "Seek command: '${cmd?.toJsonString()}'")
                        val seconds = cmd?.get("seconds") as? Double ?: (cmd?.get("seconds") as? Int)?.toDouble()
                        Log.d(TAG, "Extracted seconds: ${seconds}")
                        if (seconds != null) {
                            MediaService.seek(this@MediaService, seconds)
                        }
                    }
                    "queue" -> {
                        Log.d(TAG, "Queue command: '${cmd?.toJsonString()}'")
                        val queueJson = cmd?.get("queue") as? JsonObject
                        Thread(Runnable {
                            if (queueJson != null) {
                                MediaService.setQueue(this@MediaService, generatePlayQueue(queueJson))
                            }
                        }).start()
                    }
                    "dropQueue" -> {
                        Log.d(TAG, "Drop Queue")
                        MediaService.dropQueue(this@MediaService)
                    }
                    "info" -> {
                        Log.d(TAG, "Info command: '${cmd?.toJsonString()}'")
                        Log.d(TAG, cmd?.get("message") as? String ?: "No message found in info command: '${cmd?.toJsonString()}'")
                    }
                    "error" -> {
                        Log.d(TAG, "Error command: '${cmd?.toJsonString()}'")
                        Log.d(TAG, cmd?.get("message") as? String ?: "No message found in error command: '${cmd?.toJsonString()}'")
                    }
                }
            }
        }

        inner class NoisyAudioStreamReceiver : BroadcastReceiver() {
            val intentFilter = IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY)

            override fun onReceive(context: Context, intent: Intent) {
                if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.action)) {
                    pause()
                }
            }

            fun register() {
                registerReceiver(this, intentFilter)
            }

            fun unregister() {
                try {
                    unregisterReceiver(this)
                } catch (e: IllegalArgumentException) {
                    // do nothing
                }
            }
        }
    }
}

enum class PlayerState {
    NOT_INITIALIZED,
    STOPPED,
    PAUSED,
    PLAYING,
    LOADING,
}

fun playerStateToPlayState(ps: PlayerState): AgentAPI.PlayState {
    return when (ps) {
        PlayerState.NOT_INITIALIZED -> { AgentAPI.PlayState.STOP }
        PlayerState.STOPPED -> { AgentAPI.PlayState.STOP }
        PlayerState.PLAYING -> { AgentAPI.PlayState.PLAY }
        PlayerState.LOADING -> { AgentAPI.PlayState.STOP }
        PlayerState.PAUSED -> { AgentAPI.PlayState.PAUSE }
    }
}