package org.guodman.coordinatedmusic

import android.content.Context
import android.util.Log
import org.guodman.coordinatedmusic.model.Server

object GeneralPrefs {
    val SHARED_PREFS_NAME = "GeneralPrefs"
    val KEY_ACTIVE_SERVER_HOST = "ActiveServerHost"
    val KEY_ACTIVE_SERVER_NAME = "ActiveServerName"
    val KEY_ACTIVE_SERVER_TOKEN = "ActiveServerToken"
    val KEY_CURRENT_AGENT = "CurrentAgent"

    fun saveActiveServer(context: Context, server: Server) {
        val prefs = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.putString(KEY_ACTIVE_SERVER_HOST, server.host)
        editor.putString(KEY_ACTIVE_SERVER_NAME, server.name)
        editor.putString(KEY_ACTIVE_SERVER_TOKEN, server.token)
        editor.apply()
    }

    fun hasActiveServer(context: Context): Boolean {
        val prefs = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)
        return prefs.contains(KEY_ACTIVE_SERVER_NAME) && prefs.contains(KEY_ACTIVE_SERVER_HOST) && prefs.contains(KEY_ACTIVE_SERVER_TOKEN)
    }

    fun getActiveServer(context: Context): Server {
        val prefs = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)
        val name: String = prefs.getString(KEY_ACTIVE_SERVER_NAME, "No Host")
        val host: String = prefs.getString(KEY_ACTIVE_SERVER_HOST, "localhost")
        val token: String = prefs.getString(KEY_ACTIVE_SERVER_TOKEN, "TOKEN_NOT_FOUND")
        return Server(name, host, token)
    }

    fun saveCurrentAgent(context: Context, agentName: String) {
        Log.d(TAG, "setting agent to ${agentName}")
        val prefs = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.putString(KEY_CURRENT_AGENT, agentName)
        editor.apply()
    }

    fun getCurrentAgent(context: Context): String? {
        val prefs = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)
        return prefs.getString(KEY_CURRENT_AGENT, null)
    }
}
